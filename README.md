# Teaching an IA to fly a rocket in KSP

All the codes (jupyter notebooks) I've been tinkering with on my project to teach an IA to fly a rocket in the video game Kerbal Space Program.
[KSP forum post to learn more about the project](https://forum.kerbalspaceprogram.com/index.php?/topic/183388-i-want-to-teach-a-rocket-how-to-fly/#comments)
Feel free to use and modify any of the code (I'd just appreciate a mention if you do! I'd love to know if people are interested in this :) ).

In order to run the more complex codes, it requires:
- tensorflow-gpu and all the dependcies
- keras
- numpy

Codes:
- **Return to quicksave**: simple code to get started with kRPC, testing how to revert to launch automatically (very import part to create training data without intervention).
- **tf-gpu_learning_to_throttle**: using Keras to create very simple deep-reinforcement model. Currently, not working.

These were used locally using the kRPC mod, allowing to control the game using Python.
The end goal is to use machine learning (trying to use deep learning via Keras / TensorFlow but might go back to classical machine learning later)
to teach a rocket how to fly, using reinforcement learning.
The first step was to only give throttle controls to the rocket, (either higher or lower thrust, until the booster was empty (roughly 7sec)), and try to see
if the rocket could learn on it's own, with no player input, that full throttle would be the way to maximize altitude. Unfortunately, it's not working, for now.

The main issue I have at the moment is creating enough training data to train the model, due to RAM leakage in KSP, reverting to launch multiple hundreds of times
leads to decrease in performance over time.

But beware:
These are as stable as the Krakken, and probably will result in a few RUDs.
So, blow up Kerbals at your own risk.


If you have any questions, feel free to ask on the KSP forum, or on Twitter! (@MaxLenormand) I'd love to talk more about this project!
(Disclaimer: Jeb was harmed during experiments).